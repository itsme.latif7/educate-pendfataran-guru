<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Custom_model extends CI_Model {

        public function _get_total_trafic($month, $year){
            $query=$this->db->query("SELECT count(id) as total FROM `trafic` WHERE MONTH(created_at) = '$month' and YEAR(created_at) = '$year'");
            return $query->row_array();
        }

        public function _get_total_program($month, $year){
            $query=$this->db->query("SELECT count(id) as total FROM `trafic` WHERE MONTH(created_at) = '$month' and YEAR(created_at) = '$year' and link LIKE '%/program/detail/%' ");
            return $query->row_array();
        }
        
        public function _get_total_subscriber($month, $year){
            $query=$this->db->query("SELECT count(id) as total FROM `contact` WHERE MONTH(created_at) = '$month' and YEAR(created_at) = '$year' and type = 'subscriber'");
            return $query->row_array();
        }  
        
        public function _get_total_contact($month, $year){
            $query=$this->db->query("SELECT count(id) as total FROM `contact` WHERE MONTH(created_at) = '$month' and YEAR(created_at) = '$year' and type = 'contact'");
            return $query->row_array();
        }  
        
        public function _get_total_chart_date($month, $year){
            $query=$this->db->query("SELECT DATE(created_at) as tanggal, COUNT(id) as total FROM `trafic` WHERE MONTH(created_at) = '$month' and YEAR(created_at) = '$year' GROUP BY DATE(created_at)");
            return $query->result_array();
        }

        public function _get_total_chart_subscriber($month, $year){
            $query=$this->db->query("SELECT DATE(created_at) as tanggal, COUNT(id) as total FROM `contact` WHERE MONTH(created_at) = '$month' and YEAR(created_at) = '$year' and type = 'subscriber' ");
            return $query->result_array();
        }
        
        public function _get_total_chart_city($month, $year){
            $query=$this->db->query("SELECT city, count(id) as total FROM `contact` WHERE type = 'form_whatsapp' and MONTH(created_at) = '$month' and YEAR(created_at) = '$year' GROUP BY city LIMIT 10");
            return $query->result_array();
        }  
        
        public function _list_trafic_program($month, $year){
            $query=$this->db->query("SELECT p.id, p.name, p.slug, p.views, pc.name as category_name  FROM `program` as p JOIN program_category as pc ON pc.id = p.category_id WHERE MONTH(created_at) = '$month' and YEAR(created_at) = '$year' ORDER BY views DESC LIMIT 10");
            return $query->result_array();
        }

        public function _list_trafic_all($month, $year){
            $query=$this->db->query("SELECT link, count(id) as total FROM `trafic` WHERE MONTH(created_at) = '$month' and YEAR(created_at) = '$year' and link NOT LIKE '%/paket/detail/%' GROUP BY link LIMIT 10");
            return $query->result_array();
        }    
        
        public function _get_article_category($status="one", $id=0){
            $this->db->select("r.category_id, ac.name");
            $this->db->from("article_categgory_relation as r");
            $this->db->join('article_category as ac', 'ac.id = r.category_id');

            if($status=="one"){
                $this->db->where('r.article_id', $id);
            }

            $query=$this->db->get();
            return $query->result_array();
        }

        public function _get_upcoming_program($category_id=0){
            $date=date("Y-m-d");

            $this->db->select('p.id, p.name, p.slug, p.image, pd.start_date');
            $this->db->join('program as p', 'p.id = pd.program_id');
            $this->db->where('pd.start_date >= ', $date);
            $this->db->where('p.status', 'PUBLISH');
            
            if($category_id>0){
                $this->db->where('p.category_id', $category_id);
            }

            $this->db->limit(1);
            $this->db->from('program_date as pd');

            $query=$this->db->get();
            return $query->row_array();
        }

        public function _get_articles($offset, $limit, $type="article", $param="", $count="no"){
            $this->db->select('a.*, c.name as category_name, c.slug as category_slug');
            $this->db->join('article_category as c', 'c.id = a.category_id');
            
            
            if($type=="category"){
                $this->db->where('a.category_id', $param);
            }elseif($type=="hashtag"){
                $this->db->join('article_hashtag as ah', 'ah.article_id = a.id');
                $this->db->where('ah.slug', $param);
            }

            $this->db->where('a.status', 'PUBLISH');
            
            if($count=="yes"){
                $this->db->from('article as a');
                $query=$this->db->get();
                return $query->result_array();               
            }else{
                $this->db->limit($limit,$offset);
                $this->db->order_by('a.created_at DESC');
                $this->db->from('article as a');                
                $query=$this->db->get();
                return $query->result_array();
            }

        }

        public function _get_hashtags($limit="yes"){
            $this->db->select("count(id) as total, name, slug");
            $this->db->group_by('name');
            
            if($limit=='yes'){
                $this->db->limit(13);
            }

            $this->db->from('article_hashtag');
            $query=$this->db->get();

            return $query->result_array();
        }      

        public function _get_program_homepage($category_id=0){
            $this->db->select("pd.program_id, p.name, p.slug, p.image, pd.start_date, pd.start_time, pd.end_time, pc.name as category_name, pc.slug_url as category_slug");
            $this->db->join('program as p', 'p.id = pd.program_id');
            $this->db->join('program_category as pc', 'pc.id = p.category_id');
            $this->db->where('pd.start_date >=', date("Y-m-d"));
            $this->db->where('p.status', 'PUBLISH');

            if($category_id==1){
                $this->db->where('p.category_id', $category_id);
                $this->db->limit(6);
            }else{
                $this->db->where('p.category_id >', '1');
                $this->db->limit(9);
            }
            
            $this->db->group_by('pd.program_id');
            $this->db->order_by('start_date ASC');

            $this->db->from('program_date as pd');
            $query=$this->db->get();

            return $query->result_array();
        }

        public function _get_program_search($category_id=0, $name="", $start_date="", $end_date="", $offset="", $limit=""){
            $date=date("Y-m-d");

            $this->db->select('p.id, p.name, p.slug, p.image, pd.start_date, pd.start_time, pd.end_time, pc.name as category_name, pc.slug_url as category_slug');
            $this->db->join('program as p', 'p.id = pd.program_id');
            $this->db->join('program_category as pc', 'pc.id = p.category_id');

            if($start_date=="" and $end_date==""){
                $this->db->where('pd.start_date >= ', $date);
            }else{
                if(!empty($start_date)){
                    $this->db->where('pd.start_date >= ', $start_date);
                }

                if(!empty($end_date)){
                    $this->db->where('pd.start_date <= ', $end_date);
                }
            }

            if(!empty($name)){
                $this->db->like('p.name', $name);
            }

            $this->db->where('p.status', 'PUBLISH');
            
            if($category_id>0){
                $this->db->where('p.category_id', $category_id);
            }

            $this->db->group_by('pd.program_id');
            $this->db->order_by('pd.start_date ASC');
            
            if(!empty($limit)){
                $this->db->limit($limit,$offset);
            }
            
            $this->db->from('program_date as pd');

            $query=$this->db->get();
            return $query->result_array();
        }

        public function _get_upcoming_program_not_same($category_id=0, $date="", $program_id=0, $status="next", $last_date=""){
            //echo $date;exit();
            $this->db->select('p.id, p.name, p.slug, p.image, pd.start_date');
            $this->db->join('program as p', 'p.id = pd.program_id');
            $this->db->where('pd.start_date >= ', $date);
            $this->db->where('p.status', 'PUBLISH');
            
            if($category_id>0){
                $this->db->where('p.category_id', $category_id);
            }

            if($program_id>0){
                $this->db->where('pd.program_id <>', $program_id);
            }

            if($status=="prev"){
                $this->db->where('pd.start_date <= ', $last_date);
            }

            $this->db->group_by('pd.program_id');
            $this->db->order_by('pd.start_date ASC');
            $this->db->limit(1);
            $this->db->from('program_date as pd');

            $query=$this->db->get();
            return $query->row_array();
        }

        public function _all_search_program($field=""){
            $date=date("Y-m-d");

            $this->db->select('p.id, p.name, p.slug, p.image, pd.start_date, pd.start_time, pd.end_time, pc.name as category_name, pc.slug_url as category_slug');
            $this->db->join('program as p', 'p.id = pd.program_id');
            $this->db->join('program_category as pc', 'pc.id = p.category_id');

            $this->db->group_start();
                $this->db->like('p.name', $field);
                $this->db->or_like('pc.name', $field);
            $this->db->group_end();

            $this->db->where('p.status', 'PUBLISH');

            $this->db->group_by('pd.program_id');
            $this->db->order_by('pd.start_date ASC');
            
            $this->db->from('program_date as pd');

            $query=$this->db->get();
            return $query->result_array();
        }

        public function _all_search_city($provinsi=""){
            $query = "SELECT * FROM cities ";
            $query .= "WHERE prov_id IN ";
            $query .= "(SELECT prov_id FROM provinces WHERE ";
            
            foreach ($provinsi as $key=>$prv) {
                if($key+1 == count($provinsi)){
                    $query .= "prov_name = '$prv'"; 
                } else {
                    $query .= "prov_name = '$prv' OR ";
                }
            }

            $query .= ") ORDER BY prov_id, city_name ASC";

            $result = $this->db->query($query);
            return $result->result_array();
        }

        public function _all_search_districts($city = ""){
            $query = "SELECT * FROM districts ";
            $query .= "WHERE city_id IN ";
            $query .= "(SELECT city_id FROM cities WHERE ";
            
            foreach ($city as $key=>$cty) {
                if($key+1 == count($city)){
                    $query .= "city_name = '$cty'"; 
                } else {
                    $query .= "city_name = '$cty' OR ";
                }
            }

            $query .= ") ORDER BY city_id, dis_name ASC";

            $result = $this->db->query($query);
            return $result->result_array();            
        }

        public function _registration($start_date="", $end_date=""){
            $this->db->select("p.*");
            $this->db->select("(SELECT GROUP_CONCAT(pp.mata_pelajaran SEPARATOR ', ') FROM pendaftaran_pelajaran as pp WHERE pp.pendaftaran_id = p.id) as mata_pelajaran");
            $this->db->select("GROUP_CONCAT(CONCAT_WS(' - ', c.city_name, d.dis_name) SEPARATOR ', ') as kecamatan");

            $this->db->from('pendaftaran as p');

            $this->db->join('pendaftaran_kecamatan as pk', 'pk.pendaftaran_id = p.id');
            $this->db->join('districts as d', 'd.dis_id = pk.districts_id');
            $this->db->join('cities as c', 'c.city_id = d.city_id');

            if(!empty($start_date)){
                $this->db->where('DATE(p.created_at) >=', $start_date);
            }

            if(!empty($end_date)){
                $this->db->where('DATE(p.created_at) <=', $end_date);
            }

            $this->db->order_by('p.nama_lengkap ASC');

            $query=$this->db->get();
            return $query->result_array();            
        }

    }
?>