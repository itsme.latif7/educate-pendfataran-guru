<?php
 
class Leadsregistration_model extends CI_Model {
 
    var $table = 'pendaftaran as p';
    var $column_order = array('');
    var $column_search = array('');
    var $order = array('p.nama_lengkap'=>'asc');
 
    public function __construct()
    {
        parent::__construct();
    }
 
    private function _get_datatables_query()
    {
        $start_date=$this->input->post('start_date');
        $end_date=$this->input->post('end_date');

        $this->db->select('p.*');
        
        if(!empty($start_date)){
            $this->db->where('DATE(p.created_at) >=', $start_date);
        }

        if(!empty($end_date)){
            $this->db->where('DATE(p.created_at) <=', $end_date);
        }

        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) 
        {
            if($_POST['search']['value']) 
            {
                 
                if($i===0) 
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {   
        $this->_get_datatables_query();
        $this->db->get();
        return $this->db->count_all_results();
    }

    public function _export_regis($start_date="", $end_date=""){

        $this->db->select('b.*, p.name as program_name, pc.name as category_name');
        $this->db->select("(SELECT pd.start_date FROM program_date as pd WHERE pd.program_id = p.id GROUP BY pd.program_id ORDER BY pd.start_date ASC LIMIT 1) program_date");
        $this->db->where('b.type', 'registration');
        $this->db->join('program as p', 'p.id = b.program_id');
        $this->db->join('program_category as pc', 'pc.id = p.category_id');
        
        if(!empty($start_date)){
            $this->db->where('DATE(b.created_at) >=', $start_date);
        }

        if(!empty($end_date)){
            $this->db->where('DATE(b.created_at) <=', $end_date);
        }

        $this->db->from('contact as b');

        $query=$this->db->get();
        return $query->result_array();        
       
    }
 
}
