<?php
    class Sistem_model extends CI_Model {

        public function _validate($username, $password) {
	        $this->db->select('user.*');
	        $this->db->from('user');

            $this->db->group_start();
	            $this->db->where('user.username', $username);
                $this->db->or_where('user.email', $username);
            $this->db->group_end();

	        $this->db->where('user.password', $password);
	        $this->db->where('user.status', 'Active');

	        $query=$this->db->get();

	        if($query->num_rows()>0) return $query->row_array();
	        else return array();
        }

		public function _input($table,$data){
			$this->db->insert($table,$data);
        }       
        
        public function _update($table, $data, $condition){
            $this->db->where($condition);
            $this->db->update($table, $data);            
        }
        
        public function _delete($table, $where) {
		    $this->db->delete($table, $where);        
        }
                        
        public function _get_data($table, $order){
            $this->db->order_by($order);
            return $this->db->get($table)->result_array();
        }

        public function _get_data_wheres($table, $fieldArray, $order=""){
            $this->db->order_by($order);
            $this->db->where($fieldArray);
            return $this->db->get($table)->result_array();      
        }

        public function _get_data_id($table, $field, $value){
            $this->db->where($field, $value);
            return $this->db->get($table)->row_array();      
        }

        public function _get_data_limit($table, $limit, $order=""){
            $this->db->order_by($order);
            $this->db->limit($limit); 
            return $this->db->get($table)->result_array();
        }     

        public function _get_data_wheres_limit($table, $fieldArray, $limit, $order=""){
            $this->db->order_by($order);
            $this->db->limit($limit); 
            $this->db->where($fieldArray);
            return $this->db->get($table)->result_array();
        }     
        

        public function _get_data_limit_offset($table, $offset, $limit, $order=""){
            $this->db->order_by($order);
            $this->db->limit($limit,$offset);
            return $this->db->get($table)->result_array();
        }                


        
    }
?>