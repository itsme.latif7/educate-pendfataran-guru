    <!--<div class="site-header dsn-container dsn-load-animate">
        <div class="extend-container d-flex w-100 align-items-baseline justify-content-between align-items-end">
            <div class="inner-header p-relative">
                <div class="main-logo">
                    <a href="<?php echo base_url(); ?>" data-dsn="parallax">
                        <img class="light-logo"
                            src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                            data-dsn-src="<?php echo base_url(); ?>theme/front/assets/img/logo.png" style="height:30x; width: 150px, impor !important;" alt="" />
                        <img class="dark-logo"
                            src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                            data-dsn-src="<?php echo base_url(); ?>theme/front/assets/img/logo-dark-rep.png" alt="" />
                    </a>
                </div>
            </div>
            <div class="menu-icon d-flex align-items-baseline">
                <div class="text-menu p-relative  font-heading text-transform-upper">
                    <div class="p-absolute text-button">Menu</div>
                    <div class="p-absolute text-open">Open</div>
                    <div class="p-absolute text-close">Close</div>
                </div>
                <div class="icon-m" data-dsn="parallax" data-dsn-move="10">
                    <span class="menu-icon-line p-relative d-inline-block icon-top"></span>
                    <span class="menu-icon-line p-relative d-inline-block icon-center"></span>
                    <span class="menu-icon-line p-relative d-block icon-bottom"></span>
                </div>
            </div>
            <nav class="accent-menu dsn-container main-navigation p-absolute  w-100  d-flex align-items-baseline ">
                <div class="menu-cover-title">Menu</div>
                <ul class="extend-container p-relative d-flex flex-column justify-content-center h-100">

                    <li class="dsn-active dsn-drop-down">
                        <a href="#" class="user-no-selection">

                            <span class="dsn-title-menu">Demos</span>
                            <span class="dsn-meta-menu">01</span>
                            <span class="dsn-bg-arrow"></span>
                        </a>


                        <ul>
                            <li class="dsn-back-menu">
                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                    data-dsn-src="<?php echo base_url(); ?>theme/front/assets/img/left-chevron.svg" alt="">
                                <span class="dsn-title-menu">Demos</span>
                            </li>
                            <li>
                                <a href="index.html">
                                    <span class="dsn-title-menu">Main Demos</span>
                                    <span class="dsn-meta-menu">01</span>
                                </a>
                            </li>
                            <li>
                                <a href="demo-2.html">
                                    <span class="dsn-title-menu">Demo 2</span>
                                    <span class="dsn-meta-menu">02</span>
                                </a>
                            </li>

                            <li>
                                <a href="demo-3.html">
                                    <span class="dsn-title-menu">Demo 3</span>
                                    <span class="dsn-meta-menu">03</span>
                                </a>
                            </li>

                            <li>
                                <a href="corprate.html">
                                    <span class="dsn-title-menu">corprate</span>
                                    <span class="dsn-meta-menu">04</span>
                                </a>
                            </li>

                            <li>
                                <a href="corprate-2.html">
                                    <span class="dsn-title-menu">corprate 2</span>
                                    <span class="dsn-meta-menu">05</span>
                                </a>
                            </li>

                            <li>
                                <a href="personal.html">
                                    <span class="dsn-title-menu">personal</span>
                                    <span class="dsn-meta-menu">06</span>
                                </a>
                            </li>

                            <li>
                                <a href="personal-2.html">
                                    <span class="dsn-title-menu">personal 2</span>
                                    <span class="dsn-meta-menu">07</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="dsn-drop-down">
                        <a href="#" class="user-no-selection">

                            <span class="dsn-title-menu">Sliders parallax</span>
                            <span class="dsn-meta-menu">02</span>
                            <span class="dsn-bg-arrow"></span>
                        </a>


                        <ul>
                            <li class="dsn-back-menu">
                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                    data-dsn-src="<?php echo base_url(); ?>theme/front/assets/img/left-chevron.svg" alt="">
                                <span class="dsn-title-menu">Sliders</span>
                            </li>

                            <li>
                                <a href="slider-parallax-horizontal.html">
                                    <span class="dsn-title-menu">slider parallax horizontal</span>
                                    <span class="dsn-meta-menu">01</span>
                                </a>
                            </li>

                            <li>
                                <a href="slider-parallax-horizontal-2.html">
                                    <span class="dsn-title-menu">slider parallax horizontal 2</span>
                                    <span class="dsn-meta-menu">02</span>
                                </a>
                            </li>

                            <li>
                                <a href="slider-parallax-horizontal-3.html">
                                    <span class="dsn-title-menu">slider parallax horizontal 3</span>
                                    <span class="dsn-meta-menu">03</span>
                                </a>
                            </li>

                            <li>
                                <a href="slider-parallax-vertical.html">
                                    <span class="dsn-title-menu">slider parallax vertical</span>
                                    <span class="dsn-meta-menu">04</span>
                                </a>
                            </li>

                            <li>
                                <a href="slider-parallax-vertical-2.html">
                                    <span class="dsn-title-menu">slider parallax vertical 2</span>
                                    <span class="dsn-meta-menu">05</span>
                                </a>
                            </li>

                            <li>
                                <a href="slider-parallax-vertical-3.html">
                                    <span class="dsn-title-menu">slider parallax vertical 3</span>
                                    <span class="dsn-meta-menu">06</span>
                                </a>
                            </li>
                        </ul>

                    </li>

                    <li class="dsn-drop-down">
                        <a href="#" class="user-no-selection">

                            <span class="dsn-title-menu">Sliders webgel</span>
                            <span class="dsn-meta-menu">03</span>
                            <span class="dsn-bg-arrow"></span>
                        </a>


                        <ul>
                            <li class="dsn-back-menu">
                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                    data-dsn-src="<?php echo base_url(); ?>theme/front/assets/img/left-chevron.svg" alt="">
                                <span class="dsn-title-menu">Sliders webgel</span>
                            </li>

                            <li>
                                <a href="slider-webgel-horizontal.html">
                                    <span class="dsn-title-menu">slider webgel horizontal</span>
                                    <span class="dsn-meta-menu">01</span>
                                </a>
                            </li>

                            <li>
                                <a href="slider-webgel-horizontal-2.html">
                                    <span class="dsn-title-menu">slider webgel horizontal 2</span>
                                    <span class="dsn-meta-menu">02</span>
                                </a>
                            </li>

                            <li>
                                <a href="slider-webgel-horizontal-3.html">
                                    <span class="dsn-title-menu">slider webgel horizontal 3</span>
                                    <span class="dsn-meta-menu">03</span>
                                </a>
                            </li>

                            <li>
                                <a href="slider-webgel-vertical.html">
                                    <span class="dsn-title-menu">slider webgel vertical</span>
                                    <span class="dsn-meta-menu">04</span>
                                </a>
                            </li>

                            <li>
                                <a href="slider-webgel-vertical-2.html">
                                    <span class="dsn-title-menu">slider webgel vertical 2</span>
                                    <span class="dsn-meta-menu">05</span>
                                </a>
                            </li>

                            <li>
                                <a href="slider-webgel-vertical-3.html">
                                    <span class="dsn-title-menu">slider webgel vertical 3</span>
                                    <span class="dsn-meta-menu">06</span>
                                </a>
                            </li>
                        </ul>

                    </li>

                    <li>
                        <a href="about.html">

                            <span class="dsn-title-menu">About</span>
                            <span class="dsn-meta-menu">04</span>
                            <span class="dsn-bg-arrow"></span>
                        </a>
                    </li>

                    <li>
                        <a href="services.html">

                            <span class="dsn-title-menu">services</span>
                            <span class="dsn-meta-menu">05</span>
                            <span class="dsn-bg-arrow"></span>
                        </a>
                    </li>

                    <li class="dsn-drop-down">
                        <a href="#" class="user-no-selection">

                            <span class="dsn-title-menu">Portfolio</span>
                            <span class="dsn-meta-menu">06</span>
                            <span class="dsn-bg-arrow"></span>
                        </a>


                        <ul>
                            <li class="dsn-back-menu">
                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                    data-dsn-src="<?php echo base_url(); ?>theme/front/assets/img/left-chevron.svg" alt="">
                                <span class="dsn-title-menu">Portfolio</span>
                            </li>

                            <li>
                                <a href="work.html">
                                    <span class="dsn-title-menu">Work Hover</span>
                                    <span class="dsn-meta-menu">01</span>
                                </a>
                            </li>

                            <li>
                                <a href="work-2.html">
                                    <span class="dsn-title-menu">Work 2 Colum</span>
                                    <span class="dsn-meta-menu">02</span>
                                </a>
                            </li>

                            <li>
                                <a href="work-3.html">
                                    <span class="dsn-title-menu">Work 3 Colum</span>
                                    <span class="dsn-meta-menu">03</span>
                                </a>
                            </li>

                            <li>
                                <a href="work-grid.html">
                                    <span class="dsn-title-menu">Work grid</span>
                                    <span class="dsn-meta-menu">04</span>
                                </a>
                            </li>

                            <li>
                                <a href="work-masonry.html">
                                    <span class="dsn-title-menu">Work masonry</span>
                                    <span class="dsn-meta-menu">05</span>
                                </a>
                            </li>

                            <li>
                                <a href="work-masonry-2.html">
                                    <span class="dsn-title-menu">Work masonry 2</span>
                                    <span class="dsn-meta-menu">06</span>
                                </a>
                            </li>
                        </ul>

                    </li>

                    <li class="dsn-drop-down">
                        <a href="#" class="user-no-selection">

                            <span class="dsn-title-menu">Blog</span>
                            <span class="dsn-meta-menu">07</span>
                            <span class="dsn-bg-arrow"></span>
                        </a>


                        <ul>
                            <li class="dsn-back-menu">
                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                    data-dsn-src="<?php echo base_url(); ?>theme/front/assets/img/left-chevron.svg" alt="">
                                <span class="dsn-title-menu">Blog</span>
                            </li>

                            <li>
                                <a href="blog.html">
                                    <span class="dsn-title-menu">Blog </span>
                                    <span class="dsn-meta-menu">01</span>
                                </a>
                            </li>

                            <li>
                                <a href="blog-2.html">
                                    <span class="dsn-title-menu">Blog Style 2</span>
                                    <span class="dsn-meta-menu">02</span>
                                </a>
                            </li>

                            <li>
                                <a href="blog-3.html">
                                    <span class="dsn-title-menu">Blog Style 3</span>
                                    <span class="dsn-meta-menu">03</span>
                                </a>
                            </li>

                            <li>
                                <a href="post.html">
                                    <span class="dsn-title-menu">Single Post</span>
                                    <span class="dsn-meta-menu">04</span>
                                </a>
                            </li>

                        </ul>

                    </li>

                    <li>
                        <a href="contact.html" class="user-no-selection">

                            <span class="dsn-title-menu">Contact</span>
                            <span class="dsn-meta-menu">08</span>
                            <span class="dsn-bg-arrow"></span>
                        </a>
                    </li>
                </ul>
                <div class="container-content  p-absolute h-100 left-60 d-flex flex-column justify-content-center">
                    <div class="nav__info">
                        <div class="nav-content">
                            <p class="title-line">
                                Studio</p>
                            <p>
                                26-30 New Damietta<br>
                                El-Mahalla El-Kubra, SK1 66LM</p>
                        </div>
                        <div class="nav-content">
                            <p class="title-line">
                                Contact</p>
                            <p class="links over-hidden">
                                <a href="#" data-hover-text="+00 (2)012 3321" class="link-hover">+00 (2)012 3321</a>
                            </p>
                            <p class="links  over-hidden">
                                <a href="#" data-hover-text="info@dsngrid.com" class="link-hover">info@dsngrid.com</a>
                            </p>
                        </div>
                    </div>
                    <div class="nav-social nav-content">
                        <div class="nav-social-inner p-relative">
                            <p class="title-line">
                                Follow us</p>
                            <ul>
                                <li>
                                    <a href="#" target="_blank" rel="nofollow">Facebook.
                                        <div class="icon-circle"></div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank" rel="nofollow">Instagram.
                                        <div class="icon-circle"></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>-->