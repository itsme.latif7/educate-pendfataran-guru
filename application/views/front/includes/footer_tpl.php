                <footer class="footer p-relative background-section">
                    <div class="container">
                        <div class="footer-container">
                            <div class="d-flex align-items-center h-100">
                                <div class="column-left">
                                    <div class="footer-social p-relative">
                                        <ul>
                                            <li class="over-hidden">
                                                <a href="#" data-dsn="parallax" target="_blank"
                                                    rel="nofollow">Facebook.</a>
                                            </li>
                                            <li class="over-hidden">
                                                <a href="#" data-dsn="parallax" target="_blank"
                                                    rel="nofollow">Instagram.</a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>

                                <div class="scroll-top animation-rotate" data-dsn="parallax">
                                    <img src="<?php echo base_url(); ?>theme/front/assets/img/scroll_top.svg" alt="">
                                    <i class="fa fa-angle-up"></i>
                                </div>

                                <div class="column-right">
                                    <h5>2021 © Made with <span class="love">♥</span>by
                                        <a class="link-hover" data-hover-text="Design Grid." target="_blank"
                                            rel="nofollow"
                                            href="#">Langkah Digital Advertising.</a>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>