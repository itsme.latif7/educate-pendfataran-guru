<!DOCTYPE html>
<html lang="en-US">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="discrption" content="Sebagai Creative Design Digital Agency di Jakarta - Indonesia, kami hubungkan bisnis, merek dan pelanggan melalui strategi pemasaran untuk hasil maksimal" />
    <meta name="keyword" content="Agency Digital, Creative Digital" />

    <!--  Title -->
    <title>Langkah Digital Advertising</title>

    <!-- Font Google -->
    <link rel="preload" href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&family=Poppins:wght@300;400;500;600;700&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'">
    
    <noscript>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&family=Poppins:wght@300;400;500;600;700&display=swap"
            rel="stylesheet">
    </noscript>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>theme/front/assets/img/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="<?php echo base_url(); ?>theme/front/assets/img/favicon.ico" type="image/x-icon" />

    <link rel="preload" href="<?php echo base_url(); ?>theme/front/assets/img/circle-dotted.png" as="image" />

    <!-- custom styles (optional) -->
    <link href="<?php echo base_url(); ?>theme/front/assets/css/plugins.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/front/assets/css/style.css">

</head>