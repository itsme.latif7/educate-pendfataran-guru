<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title; ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/front/dist/style.css">
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 col-sm-12">
                <?php
                    echo form_open_multipart('home/proses');
                ?>
                <div class="card mb-3">
                <div class="card">
                    <div class="row g-0" style="padding:0;">
                      <div class="col-md-12">
                        <img src="<?php echo base_url(); ?>theme/front/images/banner-portal-mobile.jpg" class="card-img-top">
                        <div class="card-body">
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="#">Nama Lengkap</label>
                                      <input type="text" id="name" name="nama_lengkap" required class="form-control">
                                  </div>

                                  <div class="form-group">
                                        <label for="#">Email</label>
                                        <input type="email" class="form-control" required name="email">                                      
                                  </div>

                                  <div class="form-group">
                                    <label for="#">Universitas</label>
                                    <input type="text" class="form-control" name="universitas" required>                                      
                                  </div>  
                                  
                                  <div class="form-group">
                                    <label for="#">Jurusan</label>
                                    <input type="text" class="form-control" name="jurusan" required>                                      
                                  </div>  

                                  <div class="form-group">
                                    <label for="#">Upload KTP</label>
                                    <input type="file" class="form-control" name="file_ktp" accept="image/png, image/jpg, image/jpeg, application/pdf" required>                                      
                                  </div>    
                                  
                                  <div class="form-group">
                                    <label for="#">Upload CV</label>
                                    <input type="file" class="form-control" name="file_cv" accept="image/png, image/jpg, image/jpeg, application/pdf" required>                                      
                                  </div>      
                                  
                                  
                              </div>
                              
                              <div class="col-md-6">  

                                    <div class="form-group">
                                        <label for="#">No Whatsapp</label>
                                        <div class="input-group">
                                            <span class="input-group-text" id="basic-addon1">+62</span>
                                            <input type="number" class="form-control" name="no_whatsapp" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="#">Alamat Lengkap</label>
                                        <textarea rows="3" class="form-control" name="alamat_lengkap" required></textarea>
                                    </div>  
                                    
                                    <div class="form-group">
                                        <label for="#">Mata Pelajaran</label>
                                        <select class="form-select" id="mata_pelajaran"  name="mata_pelajaran[]" multiple="multiple" required>
                                            <?php
                                                foreach ($mata_pelajaran as $matpel) {
                                                    $matpelName=ucwords(strtolower($matpel['name']));
                                            ?>
                                                    <option value="<?php echo $matpel['name']; ?>"><?php echo $matpelName; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>  
                                    
                                    <label for="#">Wilayah Mengajar</label>
                                    <div class="form-group">
                                        <select id="provinsi" class="form-select" name="provinsi_mengajar" multiple="multiple"  required>
                                            <?php
                                                foreach ($provincies as $prov) {
                                                    $prov_name=ucwords(strtolower($prov['prov_name']));
                                            ?>
                                                    <option value="<?php echo $prov['prov_name']; ?>"><?php echo $prov_name; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>   
                                    
                                    <div class="form-group">
                                        <select id="kota" class="form-select" name="kota_mengajar" multiple="multiple" required>
                                        </select>
                                    </div>  
                                    
                                    <div class="form-group">
                                        <select id="kecamatan" class="form-select" name="kecamatan_mengajar[]" multiple="multiple" required>
                                        </select>
                                    </div>                                       
                              </div>
                                
                              <div class="col-12">
                                  <div class="form-group">
                                      <label>A. Goal kamu dalam 1 tahun kedepan</label>
                                      <textarea name="goals" id="" cols="30" rows="10" class="form-control" placeholder="Deskripsikan singkat tentang diri kamu dan ceritakan goals kamu kedepan untuk dirimu dan pendidikan"></textarea>
                                  </div>

                                  <div class="form-group">
                                      <label>B. Mengapa kami harus memilih kamu untuk menjadi guru inspiratif ?</label>
                                      <textarea name="why" id="" cols="30" rows="10" class="form-control" placeholder="Disini kamu harus menyakinkan kami untuk menjadikan kamu sebagai guru inspiratif. Beberapa hal yang mungkin bisa kamu tuliskan :&#13;&#10&#13;&#10Berapa lama kamu berpengalaman mengajar? &#13;&#10&#13;&#10Berapa banyak siswa yang pernah kamu ajar? Bagaimana perkembangan mereka dari (hasil yang diperoleh, nilai ujian, lulus atau tidak...... dan lainnya) &#13;&#10&#13;&#10Apa saja kekurangan dan kelebihan yang kamu miliki?"></textarea>
                                  </div>   
                                  
                                  <div class="form-group">
                                      <label>C. Bagaimana treatment terbaikmu untuk meningkatkan nilai akademik siswa ?</label>
                                      <textarea name="treatment" id="" cols="30" rows="10" class="form-control" placeholder="Disini kamu harus menguraikan beberapa hal yang berkaitan dengan :&#13;&#10&#13;&#10Teknik dan metode pembelajaran yang kamu gunakan &#13;&#10&#13;&#10Media pembelajaran apa yang sering digunakan &#13;&#10&#13;&#10Bahan ajar dan modul yang diberikan ke siswa"></textarea>
                                  </div>                                     
                              </div>
                              
                              <div class="col-12">
                                <hr>
                                <button type="submit" name="submit" class="btn btn-primary" value="submit">Daftar</button>
                              </div>
                          </div>

                        </div>
                      </div>                     
                    </div>
                </div>      
                <?php 
                    echo form_close();
                ?>          
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#mata_pelajaran").select2({
                placeholder: "Pilih Mata Pelajaran",
                allowClear: true,
                multiple: true
            });

            $('#provinsi').select2({
                placeholder: "Pilih Provinsi",
                multiple: true,   
                allowClear: true  

            });

            $("#provinsi").on('change', function(){
                $.ajax({
                    url: "<?php echo base_url(); ?>home/city/",
                    type: "post",
                    data: {name: $(this).val()} ,
                    success: function (response) {
                        $('#kota').empty();
                        console.log(response);
                        $('#kota').append(`${response}`);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                    }
                });                 
            });

            $('#kota').select2({
                placeholder: "Pilih Kota",
                multiple: true,   
                allowClear: true               
            }); 

            $("#kota").on('change', function(){
                $.ajax({
                    url: "<?php echo base_url(); ?>home/districts/",
                    type: "post",
                    data: {name: $(this).val()} ,
                    success: function (response) {
                        $('#kecamatan').empty();
                        console.log(response);
                        $('#kecamatan').append(`${response}`);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                    }
                });                 
            });  

            $('#kecamatan').select2({
                placeholder: "Pilih Kecamatan",
                allowClear: true                
            });                    
        })
    </script>
</body>
</html>