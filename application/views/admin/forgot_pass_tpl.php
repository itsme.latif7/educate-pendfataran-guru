<?php
    $base_url=base_url()."theme/admin/";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Reset Password | SDM Web Panel</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/vendors/iconfonts/ionicons/dist/css/ionicons.css">
    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/css/shared/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="<?php echo $base_url; ?>assets/images/favicon.ico" />
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
              <div class="auto-form-wrapper">
                <?php 
                    echo form_open('admin/login/reset_pass/'); 
                ?>
                    <center><h3>Reset Password</h3></center>
                    <hr>
                    
                    <?php
                        if($param==md5(1) or $param==md5(2)){
                    ?> 
                    <div class="alert <?php if($param==md5(1)){ echo 'alert-danger'; }else{ echo 'alert-success'; } ?>" role="alert">
                        <?php
                            if($param==md5(1)){
                                echo "Email not found.";
                            }else{
                                $link=base_url()."admin/login";
                                echo "Password has been reset. <a href='$link'>Login</a>";
                            }
                        ?>
                    </div> 
                    <br>
                    <?php
                        }
                    ?>

                  <div class="form-group">
                    <label class="label">Email</label>
                    <div class="input-group">
                      <input type="email" class="form-control" placeholder="Email" name="email" id="email" required>
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="mdi mdi-check-circle-outline"></i>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary submit-btn btn-block">Reset Password</button>
                  </div>
                  <div class="form-group d-flex justify-content-between">
                    <a href="<?php echo base_url(); ?>admin/login" class="text-small forgot-password text-black">Login</a>
                  </div>
                <?php
                    echo form_close();
                ?>
              </div>

            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?php echo $base_url; ?>assets/vendors/js/vendor.bundle.base.js"></script>
    <script src="<?php echo $base_url; ?>assets/vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- endinject -->
    <script src="<?php echo $base_url; ?>assets/js/shared/jquery.cookie.js" type="text/javascript"></script>
  </body>
</html>