<?php
    $base_url=base_url()."theme/admin/";
    $this->load->view('admin/includes/doctype_tpl');
?>
  </head>
  <body>
    <div class="container-scroller">
      <?php 
        $this->load->view('admin/includes/navbar_tpl');
      ?>

      <div class="container-fluid page-body-wrapper">

        <?php 
          $this->load->view('admin/includes/sidebar_tpl');
        ?>

        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">Dashboard - Web</h4>
                </div>
              </div>

            </div>
            <!-- Page Title Header Ends-->
            <div class="row">
                  <div class="col-md-3 grid-margin stretch-card average-price-card">
                    <div class="card text-white">
                      <div class="card-body">
                        <div class="d-flex justify-content-between pb-2 align-items-center">
                          <h2 class="font-weight-semibold mb-0"><?php echo $trafic['total']; ?></h2>
                          <div class="icon-holder">
                            <i class="mdi mdi-chart-line"></i>
                          </div>
                        </div>
                        <div class="d-flex justify-content-between">
                          <h5 class="font-weight-semibold mb-0">Trafic All</h5>
                          <!--<p class="text-white mb-0">Since last month</p>-->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3 grid-margin stretch-card average-price-card">
                    <div class="card text-white">
                      <div class="card-body red">
                        <div class="d-flex justify-content-between pb-2 align-items-center">
                          <h2 class="font-weight-semibold mb-0"><?php echo $program['total']; ?></h2>
                          <div class="icon-holder">
                            <i class="mdi mdi-view-grid"></i>
                          </div>
                        </div>
                        <div class="d-flex justify-content-between">
                          <h5 class="font-weight-semibold mb-0">Trafic Program</h5>
                         <!-- <p class="text-white mb-0">Since last month</p>-->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3 grid-margin stretch-card average-price-card ">
                    <div class="card text-white">
                      <div class="card-body green">
                        <div class="d-flex justify-content-between pb-2 align-items-center">
                          <h2 class="font-weight-semibold mb-0"><?php echo $subscriber['total']; ?></h2>
                          <div class="icon-holder">
                            <i class="mdi mdi-email"></i>
                          </div>
                        </div>
                        <div class="d-flex justify-content-between">
                          <h5 class="font-weight-semibold mb-0">Subscriber</h5>
                          <!--<p class="text-white mb-0">Since last month</p>-->
                        </div>
                      </div>
                    </div>
                  </div>                  

                  <div class="col-md-3 grid-margin stretch-card average-price-card">
                    <div class="card text-white">
                      <div class="card-body ocean">
                        <div class="d-flex justify-content-between pb-2 align-items-center">
                          <h2 class="font-weight-semibold mb-0"><?php echo $contact['total']; ?></h2>
                          <div class="icon-holder">
                            <i class="mdi mdi-forum"></i>
                          </div>
                        </div>
                        <div class="d-flex justify-content-between">
                          <h5 class="font-weight-semibold mb-0">Contact</h5>
                          <!--<p class="text-white mb-0">Since last month</p>-->
                        </div>
                      </div>
                    </div>
                  </div>
            </div>

            <div class="row">
              <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title mb-0">Statistics of Trafics Website</h4>
                    <div class="d-flex flex-column flex-lg-row">
                      <p>The number of visitors is counted per page loaded</p>
                    </div>
                    <div class="d-flex flex-column flex-lg-row">
                      <div class="ml-lg-auto" id="sales-statistics-legend"></div>
                    </div>
                    <canvas id="myChart_TraficWeb" style="width:100%"></canvas>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
                  <div class="col-md-12 grid-margin">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title mb-0">Statistics of Subscriber</h4>
                          <div class="d-flex flex-column flex-lg-row">
                            <p>The number of submissions is calculated when filling out the form.</p>
                          </div>

                          <!--<div class="d-flex align-items-end">
                            <h3 class="mb-0 font-weight-semibold">$36,2531.00</h3>
                            <p class="mb-0 font-weight-medium mr-2 ml-2 mb-1">USD</p>
                            <p class="mb-0 text-success font-weight-semibold mb-1">(+1.37%)</p>
                          </div>-->
                          <canvas id="myChart_Submission" style="width:100%"></canvas>
                        </div>
                      </div>
                  </div>
            </div>     

            <div class="row">
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-12 grid-margin">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between">
                          <h4 class="card-title mb-0">Programs View</h4>
                        </div>
                        <p>List number of program frequently viewed by visitors</p>
                        <div class="table-responsive">
                          <table class="table table-striped table-hover">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Link</th>
                                <th style="text-align:right">Views</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                  $no=1;
                                  foreach ($listTraficProgram as $ltp) {
                              ?>
                              <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $ltp['name']; ?> - (<?php echo $ltp['category_name']; ?>)</td>
                                <td><a href="<?php echo base_url(); ?>program/detail/<?php echo $ltp['slug']; ?>" target="_blank"><?php echo $ltp['slug']; ?></a></td>
                                <td style="text-align:right"><?php echo $ltp['views']; ?></td>
                              </tr>
                              <?php
                                  }
                              ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 grid-margin">
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-between">
                          <h4 class="card-title mb-0">Pages View</h4>
                        </div>
                        <p>List number of pages frequently viewed by visitors</p>
                        <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Page</th>
                                <th style="text-align:right">Views</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                  $no=1;
                                  foreach ($listTraficAll as $lta) {
                              ?>
                              <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $lta['link']; ?></td>
                                <td style="text-align:right"><?php echo $lta['total']; ?></td>
                              </tr>
                              <?php
                                  }
                              ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12 grid-margin">
                    <div class="card">
                      <div class="card-body">
                      <label>Upcoming Program </label><br>
                      <hr>
                        <h1 class="card-title mb-4"><?php echo $upcomingProgram['name']; ?></h1>
                        <div class="row">
                          <div class="col-12 col-md-12">
                              <img src="<?php echo base_url(); ?>theme/uploads/programs/<?php echo $upcomingProgram['image']?>" width="100%">
                          </div>
                          <div class="col-12 col-md-12">
                          <label><strong>Schedule</strong></label>
                            <table class="table table-hover" id="datatables">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=1;
                                        foreach ($programDate as $dateVal) {
                                    ?>
                                            <tr>
                                                <td><?php echo $no++; ?></td>
                                                <td><?php echo $dateVal['start_date']; ?></td>
                                                <td><?php echo $dateVal['start_time']; ?> - <?php echo $dateVal['end_time']; ?></td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table> 
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <?php
              $this->load->view('admin/includes/footer_tpl');
          ?>

        </div>

      </div>

    </div>
    <?php
        $this->load->view('admin/includes/script_tpl');
        $lastDate=date('t');

        $arrayDate=array();
        for ($i=1; $i <= $lastDate ; $i++) { 
            $day=$i;
            if(strlen($day)==1){
              $zero=0;
            }else{
              $zero="";
            }

            $createDate=date("Y-m-").$zero.$day;
            $arrayDate[$createDate]['tanggal']=$createDate;
        }

        //echo print_r($arrayDate)."<br><br>";
        //echo print_r($traficWhatsapp);


    ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>

    <script>

      var ctx = document.getElementById('myChart_TraficWeb').getContext('2d');
      var myChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: [<?php foreach ($arrayDate as $aD) { echo "'".date('d', strtotime($aD['tanggal']))."',"; }?>],
              datasets: [{
                  label: '# Jumlah Kunjungan',
                  data: [<?php foreach ($arrayDate as $aD) { if(isset($traficArray[$aD['tanggal']]['total_trafic'])){ echo "'".$traficArray[$aD['tanggal']]['total_trafic']."',"; } else {echo "'0',"; } }?>],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      'rgba(75, 192, 192, 0.2)',
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              responsive: false,
              maintainAspectRatio: false
          }
      });

      var ctxSubmission = document.getElementById('myChart_Submission').getContext('2d');
      var myChartSubmission = new Chart(ctxSubmission, {
          type: 'line',
          data: {
              labels: [<?php foreach ($arrayDate as $aD) { echo "'".date('d', strtotime($aD['tanggal']))."',"; }?>],
              datasets: [{
                  label: '# Total Subscriber',
                  data: [<?php foreach ($arrayDate as $aD) { if(isset($traficSubscriber[$aD['tanggal']]['total_subscriber'])){ echo "'".$traficSubscriber[$aD['tanggal']]['total_subscriber']."',"; } else {echo "'0',"; } }?>],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      'rgba(75, 192, 192, 0.2)',
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              responsive: false,
              maintainAspectRatio: false
          }
      });      

    </script> 

  </body>
</html>