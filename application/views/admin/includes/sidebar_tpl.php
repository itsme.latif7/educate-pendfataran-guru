<?php
    $base_url=base_url()."theme/admin/";
?>
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">

            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="profile-image">
                  <img class="img-xs rounded-circle" src="<?php echo $base_url; ?>assets/images/faces/face8.jpg" alt="profile image">
                  <div class="dot-indicator bg-success"></div>
                </div>
                <div class="text-wrapper">
                  <p class="profile-name"><?php echo $this->session->userdata('fullname');?></p>
                  <p class="designation">Admin</p>
                </div>
              </a>
            </li>
            
            <li class="nav-item nav-category">Main Menu</li>

            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>admin/leads/">
                <i class="menu-icon typcn typcn-document-text"></i>
                <span class="menu-title">Leads</span>
              </a>
            </li>
          </ul>
    </nav>