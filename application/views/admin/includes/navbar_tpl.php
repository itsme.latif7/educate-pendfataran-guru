<?php
    $base_url=base_url()."theme/admin/";
?>
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
          <a class="navbar-brand brand-logo" href="#">
            ADMIN PANEL</a>
          <a class="navbar-brand brand-logo-mini" href="#">
          ADMIN PANEL </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
          <ul class="navbar-nav ml-auto">
          <!--<li><a class="dropdown-item" href="<?php echo base_url(); ?>admin/profile/"><i class="mdi mdi-account-box"></i> Profile<i class="dropdown-item-icon ti-power-off"></i></a></li>-->
            <li><a class="dropdown-item" href="<?php echo base_url(); ?>admin/logout/"><i class="mdi mdi-logout-variant"></i> Sign Out<i class="dropdown-item-icon ti-power-off"></i></a></li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
    </nav>