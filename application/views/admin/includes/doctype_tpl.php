<?php
    $base_url=base_url()."theme/admin/";
?>
<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title><?php if(isset($title)) echo $title; ?> | Panel Admin - Pendaftaran Guru</title>

    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/vendors/iconfonts/ionicons/dist/css/ionicons.css">
    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/vendors/css/vendor.bundle.addons.css">
    
    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/css/shared/style.css">

    <link rel="stylesheet" href="<?php echo $base_url; ?>assets/css/demo_1/style.css">
    
    <link rel="shortcut icon" href="<?php echo $base_url; ?>assets/images/favicon.ico" />