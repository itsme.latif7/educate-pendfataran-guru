<?php
    $base_url=base_url()."theme/admin/";
    $this->load->view('admin/includes/doctype_tpl');
?>
    <link href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>theme/admin/assets/css/demo_1/loading.css" rel="stylesheet">
  </head>
  <body>
    <div class="container-scroller">
      <?php 
        $this->load->view('admin/includes/navbar_tpl');
      ?>

      <div class="container-fluid page-body-wrapper">

        <?php 
          $this->load->view('admin/includes/sidebar_tpl');
        ?>

        <div class="main-panel">
          <div id="loading">
              <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
          </div>      

          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">Leads - Registration</h4>
                </div>
              </div>

            </div>

            <div class="row">
                    <div class="col-lg-12">
                        <?php
                          echo form_open('');
                        ?>
                        <fieldset style="width:100%; background:#eeeeee; padding:10px; margin-bottom:50px;">
                            <legend>Filter :</legend>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <input type="date" class="form-control" name="start" id="start_date" placeholder="Dari" value="<?php echo $start; ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label>Until Date</label>
                                        <input type="date" class="form-control" name="end" id="end_date" placeholder="Sampai" value="<?php echo $end; ?>">
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                       <br>
                                        <button type="submit" name="submit" id="filter" value="filter" class="btn btn-secondary btn-fw"><i class="icon-search"></i>&nbsp;Filter</button>&nbsp;&nbsp;&nbsp;
                                        <button type="button" name="export" id="export" value="export" class="btn btn-primary btn-fw"><i class="icon-trash"></i>&nbsp;Export CSV</button>
                                    </div>
                                </div>
                            </div>                            
                        </fieldset>
                        <?php
                          echo form_close('');
                        ?>                        
                    </div>

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="datatables">
                        <thead>
                            <tr>
                            <th width="5%">#</th>
                            <th>Nama</th>
                            <th>No. WA</th>
                            <th>Email</th>
                            <th>Universitas</th>
                            <th>File Pendukung</th>
                            <th>Mata Pelajaran</th>
                            <th>Alamat</th>
                            <th>Tempat Mengajar</th>
                            <th>Dibuat</th>
                            <th>Ip Address</th>
                            <th>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                              $no=1;
                              foreach ($registrations as $regis) {
                                  $linkKTP=base_url()."assets/uploads/".$regis['file_ktp'];
                                  $linkCV=base_url()."assets/uploads/".$regis['file_cv'];
                            ?>
                                <tr>
                                  <td><?php echo $no++; ?></td>
                                  <td><?php echo $regis['nama_lengkap']; ?></td>
                                  <td><?php echo $regis['no_whatsapp']; ?></td>
                                  <td><?php echo $regis['email']; ?></td>
                                  <td><?php echo $regis['universitas']; ?> / <?php echo $regis['jurusan']; ?></td>
                                  <td><a href="<?php echo $linkKTP; ?>" target="_blank">FILE KTP</a> / <a href="<?php echo $linkCV; ?>" target="_blank">FILE CV</a></td>
                                  <td><?php echo $regis['mata_pelajaran']; ?></td>
                                  <td><?php echo $regis['alamat']; ?></td>
                                  <td><?php echo $regis['kecamatan']; ?></td>
                                  <td><?php echo $regis['created_at']; ?></td>
                                  <td><?php echo $regis['ip_address']; ?></td>
                                  <td><button class="btn btn-success btn-detail" id="<?php echo $regis['id']; ?>" data-toggle="modal" data-target="#exampleModal">Detail</button></td>
                                </tr>
                            <?php
                              }
                            ?>
                        </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail Pendaftaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>          
          <?php
              $this->load->view('admin/includes/footer_tpl');
          ?>

        </div>

      </div>

    </div>
    <?php
        $this->load->view('admin/includes/script_tpl');
    ?>
   <script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
   <script>
        $(function () {
            table = $('#datatables').DataTable({ 
            });

            $("#loading").hide();

            $("#export").click(function(){
                var start_date = $("#start_date").val();
                var end_date = $("#end_date").val();     
                var url="<?php echo base_url(); ?>admin/leads/export_registration/"+start_date+"/"+end_date;
                
                window.open(url, '_blank').focus();                 
            });

            $(".btn-detail").click(function(){
              console.log('test');
              let id = $(this).attr('id');
              $.get("<?php echo base_url(); ?>admin/leads/detail/"+ id, function(data){
                $(".modal-body").empty();
                $(".modal-body").html(data);
              });
            });

        });
   </script>    
  </body>
</html>