<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
        parent :: __construct();
        time_cek();
        $this->load->model('sistem_model');
        $this->load->model('custom_model');
	}

    public function index(){
        $data['title']="Pendaftaran Guru - Educate";
        
        $data['provincies']=$this->sistem_model->_get_data('provinces', 'prov_name ASC');
        $data['mata_pelajaran']=$this->sistem_model->_get_data('mata_pelajaran', 'name ASC');
        $this->load->view('front/home_tpl', $data);
    }

    public function proses(){
        $submit=$this->input->post('submit');
        if($submit=="submit"){
            $this->db->trans_start();

                $name=$this->input->post('nama_lengkap');
                $email = $this->input->post('email');
                $no_whatsapp = $this->input->post('no_whatsapp');

                $ucode= md5(mt_rand(9999, 10000).get_client_ip().$email.$no_whatsapp.date('YmdHis'));

                $path = $_FILES['file_ktp']['name'];
                $newName = create_slug(strtolower($name))."-ktp-".date('YmdHis').".".pathinfo($path, PATHINFO_EXTENSION); 

                $config['file_name'] = $newName;
                $config['upload_path'] = './assets/uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 2000;
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
        
                if (!$this->upload->do_upload('file_ktp')) 
                {
                    $error = array('error' => $this->upload->display_errors());
                    echo print_r($error);
                } 
                else 
                {
                    $file_ktp = $config['file_name'];
                }     
                
                $pathCV = $_FILES['file_cv']['name'];
                $newNameCV = create_slug(strtolower($name))."-cv-".date('YmdHis').".".pathinfo($pathCV, PATHINFO_EXTENSION); 

                $configCV['file_name'] = $newNameCV;
                $configCV['upload_path'] = './assets/uploads/';
                $configCV['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $configCV['max_size'] = 2000;
                
                $this->load->library('upload', $configCV);
                $this->upload->initialize($configCV);
        
                if (!$this->upload->do_upload('file_cv')) 
                {
                    $error = array('error' => $this->upload->display_errors());
                    echo print_r($error);
                } 
                else 
                {
                    $file_cv = $configCV['file_name'];
                }       
                
                $inp['nama_lengkap'] = $name;
                $inp['no_whatsapp'] = $this->input->post('no_whatsapp');;
                $inp['email'] = $this->input->post('email');
                $inp['universitas'] = $this->input->post('universitas');;
                $inp['jurusan'] = $this->input->post('jurusan');
                $inp['file_ktp'] = $file_ktp;
                $inp['file_cv'] = $file_cv;
                $inp['alamat'] = $this->input->post('alamat_lengkap');
                $inp['goals'] = $this->input->post('goals');
                $inp['why'] = $this->input->post('why');
                $inp['treatment'] = $this->input->post('treatment');
                $inp['created_at'] = date('Y-m-d H:i:s');
                $inp['ip_address'] = get_client_ip();
                $inp['ucode'] = $ucode;

                $this->sistem_model->_input('pendaftaran', $inp);
                $getID = $this->sistem_model->_get_data_id('pendaftaran', 'ucode', $ucode);

                $mata_pelajaran = $this->input->post('mata_pelajaran');
                foreach ($mata_pelajaran as $key_matpel => $val_matpel) {
                    $inpMatPel['pendaftaran_id'] = $getID['id'];
                    $inpMatPel['mata_pelajaran'] = $val_matpel;

                    $this->sistem_model->_input('pendaftaran_pelajaran', $inpMatPel);
                }

                $kecamatan_mengajar = $this->input->post('kecamatan_mengajar');
                foreach ($kecamatan_mengajar as $key_kecamatan => $val_matpel) {
                    $inpKec['pendaftaran_id'] = $getID['id'];
                    $inpKec['districts_id'] = $val_matpel;
                    
                    $this->sistem_model->_input('pendaftaran_kecamatan', $inpKec);
                }                
                
            $this->db->trans_complete();

            echo print_r($inp);

            //redirect('berhasil');
        }
    }

    public function berhasil(){
        $this->load->view('front/berhasil');
    }

    public function city(){
        //echo print_r($_POST);
        $name = $this->input->post('name');
        $data['cities'] = $this->custom_model->_all_search_city($name);

        $this->load->view('front/list_cities_tpl', $data);
    }

    public function districts(){

        $name = $this->input->post('name');
        $data['districts'] = $this->custom_model->_all_search_districts($name);
        
        $this->load->view('front/list_districts_tpl', $data);
    }    
}

?>