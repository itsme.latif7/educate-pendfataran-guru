<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berhasil extends CI_Controller {

	function __construct()
	{
        parent :: __construct();
        time_cek();
        $this->load->model('sistem_model');
	}

    public function index(){
        $data['title']="Pendaftaran Guru - Educate";
        $this->load->view('front/berhasil_tpl', $data);
    }
}

?>