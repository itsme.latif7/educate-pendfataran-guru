<?php
	/**
	* 
	*/
	class Logout extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('sistem_model');
			time_cek();
			is_logged_in();
			cache_login();
		}

	 	public function index(){
		 	$this->db->trans_start();		 	
				$this->session->sess_destroy();
		 		$this->db->trans_complete();	
		 	redirect('admin/login');			 		
	 	}	

	}
?>