<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent :: __construct();
		time_cek();
		$this->load->model('sistem_model');	
	}

	public function index($param="")
	{	
		$data['param']=$param;
        $this->load->view('admin/login_tpl', $data);
	}

	public function validate()
	{
		$username=$this->input->post('username');
		$password=md5($this->input->post('password'));

		$query= $this->sistem_model->_validate($username, $password);
		if($query){
				$data=array();
				$data['user_id']=$query['id'];
				$data['fullname']=$query['fullname'];
				$data['is_logged_in']=true;

				$this->session->set_userdata($data);

				redirect('admin/leads');
				
		}else{
			$this->session->set_flashdata('msg','Invalid Email and Password');
			redirect('admin/login/index/'.md5(3));
		}
	}

	/*public function forgot($param=""){
		$data['param']=$param;
		$this->load->view('admin/forgot_pass_tpl', $data);
	}

	public function reset_pass(){
		$email=$this->input->post('email');
		$ceksEmail=$this->sistem_model->_get_data_id('users', 'email', $email);
		
		if(count($ceksEmail)>0){
			$upd['password']=md5('AdminSDM123');
			$upd['updated_at']=date('Y-m-d H:i:s');
			$upd['ip_address']=get_client_ip();

			$this->sistem_model->_update('users', $upd, array('id'=>$ceksEmail['id']));
			redirect('admin/login/forgot/'.md5(2));
		}else{
			redirect('admin/login/forgot/'.md5(1));
		}
	}*/
}
