<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leads extends CI_Controller {

	function __construct()
	{
        parent :: __construct();
        time_cek();
        is_logged_in();
        cache_login();
        $this->load->model('custom_model');
        $this->load->model('sistem_model');	
	}

	public function index()
	{	
        $data['page']="Data Pendaftaran";
        $data['title']="Data Pendaftaran";

        $data['start']=$this->input->post('start');
        $data['end']=$this->input->post('end');
        
        $data['registrations']=$this->custom_model->_registration($data['start'], $data['end']);

        $this->load->view('admin/leads/registration_tpl', $data);
    }

    public function detail($id=0){
        $data['detail'] = $this->sistem_model->_get_data_id('pendaftaran', 'id', $id);
        $this->load->view('admin/leads/detail_leads_tpl', $data);
    }

    public function export_registration($start_date="", $end_date=""){
        $dataSubs=$this->custom_model->_registration($start_date, $end_date);        
        
        $listName = array( 
            ['No', 'Nama Lengkap', 'No. Whatsapp', 'Email', 'Universitas', 'Jurusan', 'File KTP', 'File CV', 'Mata Pelajaran', 'Alamat', 'Kecamatan', 'Goals', 'Why', 'Treatment', 'Dibuat', 'Ip Address']
        );
        
        $fieldName = array('nama_lengkap', 'no_whatsapp', 'email', 'universitas', 'jurusan', 'file_ktp', 'file_cv', 'mata_pelajaran', 'alamat', 'kecamatan', 'goals', 'why', 'treatment', 'created_at', 'ip_address');

        $startNumber=1;
        for ($i=$startNumber; $i < count($dataSubs)+$startNumber; $i++) { 

            $listName[$i][0]=$startNumber;

            for ($fieldNo=1; $fieldNo < count($fieldName); $fieldNo++) { 
                $listName[$i][$fieldNo] = $dataSubs[$i-$startNumber][$fieldName[$fieldNo-1]];
            }
        }

        $name="Export_Leads_Registration_".date('Y-m-d');

        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"$name".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");

        $delimiter=';';

        $handle = fopen('php://output', 'w');

        foreach ($listName as $data_array) {
           fputcsv($handle, $data_array, $delimiter);
        }
           fclose($handle);

        exit;    
    } 

    /*public function AjxDataRegistration(){
        $this->load->model('datatables/leadsregistration_model');	
        $list = $this->leadsregistration_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

            $urlKTP=base_url()."assets/uploads/".$field->file_ktp;
            $urlCV=base_url()."assets/uploads/".$field->file_cv;
            $linkKTP="<a href='$urlKTP' target='_blank'>FILE KTP</a>";
            $linkCV="<a href='$urlCV' target='_blank'>FILE CV</a>";

            $no++;
            $row = array();            
            $row[] = $no;
            $row[] = $field->nama_lengkap;
            $row[] = $field->no_whatsapp;
            $row[] = $field->email;
            $row[] = $field->universitas;
            $row[] = $linkKTP." / ".$linkCV;
            $row[] = $field->mata_pelajaran;
            $row[] = $field->alamat;
            $row[] = $field->provinsi_mengajar." / ".$field->kota_mengajar." / ".$field->kecamatan_mengajar;
            $row[] = $field->created_at;
            $row[] = $field->ip_address;
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->leadsregistration_model->count_all(),
                        "recordsFiltered" => $this->leadsregistration_model->count_filtered(),
                        "data" => $data,
                );

        echo json_encode($output);        
    } */ 
}

?>