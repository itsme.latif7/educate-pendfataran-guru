<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    function sosmed(){
        $db=get_instance();

        $db->load->model('sistem_model');
        $list=$db->sistem_model->_get_data('social_media', 'id ASC');

        return $list;
    }

    function configuration_val($title=""){
        $db=get_instance();

        $db->load->model('sistem_model');
        $list=$db->sistem_model->_get_data_id('configuration', 'title', $title);

        return $list;
    }   
    
    function configuration_vals($arrayWhere=""){
        $db=get_instance();

        $db->load->model('sistem_model');
        $list=$db->sistem_model->_get_data_wheres('configuration', $arrayWhere);

        return $list;
    }       
?>